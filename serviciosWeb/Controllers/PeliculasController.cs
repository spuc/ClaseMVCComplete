﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using serviciosWeb.Models;

namespace serviciosWeb.Controllers
{
    public class PeliculasController : ApiController
    {
        private peliculas1Entities db = new peliculas1Entities();

        // GET api/Peliculas
        public IEnumerable<Peliculas> GetPeliculas()
        {
            return db.Peliculas.AsEnumerable();
        }

        // GET api/Peliculas/5
        public Peliculas GetPeliculas(int id)
        {
            Peliculas peliculas = db.Peliculas.Find(id);
            if (peliculas == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return peliculas;
        }

        // PUT api/Peliculas/5
        public HttpResponseMessage PutPeliculas(int id, Peliculas peliculas)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != peliculas.id)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(peliculas).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Peliculas
        public HttpResponseMessage PostPeliculas(Peliculas peliculas)
        {
            if (ModelState.IsValid)
            {
                db.Peliculas.Add(peliculas);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, peliculas);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = peliculas.id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Peliculas/5
        public HttpResponseMessage DeletePeliculas(int id)
        {
            Peliculas peliculas = db.Peliculas.Find(id);
            if (peliculas == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Peliculas.Remove(peliculas);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, peliculas);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}